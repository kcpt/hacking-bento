/* IE POLYFILLS.. UNFORTUNATELY -----------
 * ------------- */

// IE .includes() polyfill
if (!String.prototype.includes) {
    String.prototype.includes = function() {
        'use strict';
        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}

// IE .remove() polyfill
(function(arr) {
    arr.forEach(function(item) {
        if (item.hasOwnProperty('remove')) {
            return;
        }
        Object.defineProperty(item, 'remove', {
            configurable: true,
            enumerable: true,
            writable: true,
            value: function remove() {
                this.parentNode.removeChild(this);
            }
        });
    });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);


/* INSERT MOBILE LIVE TV & PASSPORT LINKS -----------
 * ------------- */
$nav = document.getElementsByClassName('navbar-nav menu__ul');
// live tv
$liveLI = document.createElement('li');
$liveLI.id = "liveTVMobile";
$liveLI.className = "menu__li";
$liveLI.style = "display:none;";
$liveLI.innerHTML = '<a class="menu-item" href="https://www.kansascitypbs.org/kansas-city-pbs-livestream/" target="_blank"><span>Live TV</span></a>';
// passport
$passLI = document.createElement('li');
$passLI.id = "passportMobile";
$passLI.className = "menu__li";
$passLI.style = "display:none;";
$passLI.innerHTML = '<a class="menu-item" href="https://www.kansascitypbs.org/support/passport/" target="_blank"><img alt="Passport Link" src="https://bento.pbs.org/prod/filer_public/kcpt/Passport/e5704d7af1_KCPBS_Passport_Logo_White-small.png"></a>';
// inject
$nav[0].append($liveLI, $passLI);

/* Funding Drive Programs search filter
-- For this to work, the following elements need to be added via Bento component on the funding-drive page
--- * the filter data-id should be replaced by the current container id for the list of items
<form><input id="fd-programs-filter" type="text" placeholder="Search programs" onkeyup="fdProgramsFilter()"></form>
<filter data-id="layout-26f7c1bf-f537-4761-85a0-3ce1e0c40c96"></filter>
--------------------------------------------------------------------*/
search = document.getElementById("fd-programs-filter");
if (search) {
    fdProgramsFilter = function() {
        // Declare variables
        var input, filter, list, programs, program, artist, i, el, t, ss, noMatch = [],
            mpty;
        input = search;
        filter = input.value.toUpperCase();
        listEL = document.getElementsByTagName('filter');
        listID = listEL[0].getAttribute("data-id");
        list = document.getElementById(listID);
        cols = programs = list.querySelectorAll("div[id^='column']");
        programs = list.querySelectorAll(".component");
        el = document.createElement("span");
        el.setAttribute("id", "nomatch");
        t = document.createTextNode("Your search does not match anything.");
        el.appendChild(t);

        // Loop through all programs, and hide those who don't match the search query
        for (i = 0; i < programs.length; i++) {
            program = programs[i].querySelector("h3");
            if (program) {
                programMatch = program.innerHTML.toUpperCase().indexOf(filter) > -1;
                // console.log('program match is '+programMatch);
                if (programMatch) {
                    programs[i].style.display = "";
                    noMatch.push(true);
                } else {
                    programs[i].style.display = "none";
                    noMatch.push(false);
                }
            }
        }
        // If column children are hidden, hid column as well
        for (i = 0; i < cols.length; i++) {
            if (checkEmptyCol(cols[i])) {
                cols[i].style.display = "none";
            } else {
                cols[i].style.display = "";
            }
        }
        if (!noMatch.includes(true)) {
            if (!document.contains(document.getElementById("nomatch"))) {
                list.appendChild(el);
                // console.log('nomatch added');
            }
        } else {
            if (document.contains(document.getElementById("nomatch"))) {
                // console.log('nomatch removed');
                document.getElementById("nomatch").remove();
            }
        }
    }

    function checkEmptyCol(col) {
        var mmpty = [];
        // If column children are hidden, hide column as well
        c = col.querySelectorAll(".component");
        for (i = 0; i < c.length; i++) {
            if (getComputedStyle(c[i], null).display == "none") {
                mmpty.push(true);
            } else {
                mmpty.push(false);
            }
        }
        // console.log(mmpty);
        if (!mmpty.contains(false)) {
            mmpty = true;
        } else {
            mmpty = false;
        }
        return mmpty;
    }
}

/**
 * Array.prototype.[method name] allows you to define/overwrite an objects method
 * needle is the item you are searching for
 * this is a special variable that refers to "this" instance of an Array.
 * returns true if needle is in the array, and false otherwise
 */
Array.prototype.contains = function(needle) {
    for (i in this) {
        if (this[i] == needle) return true;
    }
    return false;
}

/*** Google Contact Form **/
// Found on Bento contact page and possibly all places where
// a Google Form is being used.
$googleForm = document.getElementById('googleFormsEmbed');
if ($googleForm) {

    var $htm = document.getElementsByTagName('html');
    var $btn = document.getElementById('googleFormsPopup');

    openForm = function() {
        $btn.classList.add('formOpen');
        $googleForm.classList.add('isOpen');
        $htm[0].style.overflow = 'hidden';
    }

    closeForm = function() {
        $btn.classList.remove('formOpen');
        $googleForm.classList.remove('isOpen');
        $htm[0].style.overflow = '';
    }

    $googleForm.addEventListener("click", closeForm, false);

}

/* --------------
DONATE POPUP JS  ----------------- */

// LOCAL STORAGE
if (typeof(Storage) !== "undefined") {
    if (localStorage.popupCheck) {
        var lcSub = localStorage.popupCheck;
        console.log('subcheck = ' + lcSub);
        var now = new Date().getTime();
        // Should we show the popup yet?
        if (lcSub > now) {
            // Action has been taken on the popup less than an hour ago
            // do nothing
            console.log('hold steady....')
        } else {
            // Yes, show the popup
            console.log('Time to activate donate popup...');
            donatePopup();
        }
    } else {
        console.log('Activating donate popup');
        donatePopup();
    }
} else {
    console.log('Show the donating popup');
    donatePopup();
}

// Popup shows only for returning visitors
function donatePopup() {
    // Auto switch lightbox images and links
    var activeTimes = [
            ['03-01-25 00:01', '03-16-25 23:59']
        ],
        activeLinks = [
            'https://kcpt.pledgecart.org/?campaign=057F4DFA-46D7-4D37-98B7-637399C9E321&source='
        ],
        activeImages = [
            'https://bento.pbs.org/prod/filer_public/kcpt/Funding%20Drive/Lightbox/1e3f4ca546_2025_KCPBS_pledgecampaign_lightbox_800x800-02_1_v02.gif'
        ],
        lightboxTitle = [
            'Spring 2025 Pledge Drive'
        ],
        rightNow = Date.now(),
        activeArray = false;

    for (x = 0; x < activeTimes.length; x++) {
        var start = new Date(activeTimes[x][0]),
            end = new Date(activeTimes[x][1]);

        if (rightNow > start.getTime() && rightNow < end.getTime()) {
            activeArray = x;
            break;
        }
    }

    // don't show donate popup on Donate page
    href = window.location.href;
    if (activeArray !== false && href.indexOf('donate') < 0) {
        var $body = document.getElementsByTagName('body'),
            donateHref = activeLinks[activeArray],
            donateBtn = document.createElement('a'),
            showDonateBtn = false,
            pop = document.createElement('section');

        donateBtn.classList.add('donate');
        donateBtn.href = donateHref;
        donateBtn.innerText = 'Donate';
        pop.id = 'donatePopup';
        $body[0].appendChild(pop);
        // delay popup from showing immediately
        setTimeout(function() {
            pop.innerHTML = "<div class='contain' style='background-image:url(" + activeImages[activeArray] + ")'><i class='fas fa-times'></i><a href='" + donateHref + "' class='donateImg'><img src='" + activeImages[activeArray] + "' alt='' /></a></div>";
            // if( showDonateBtn )

            setTimeout(function() {
                $body[0].classList.add('popupOpen');

                document.querySelector('#donatePopup i.fa-times').onclick = function() {
                    var onehour = new Date().setHours(new Date().getHours() + 1);
                    $body[0].classList.remove('popupOpen');
                    if (typeof(Storage) !== "undefined") {
                        localStorage.popupCheck = onehour;
                    }
                    // send event to GA
                    ga("send", "event", "Lighboxes", "Closed Popup", lightboxTitle[activeArray], {
                        hitCallback: function() {
                            console.log('event sent');
                        }
                    });
                }

                document.querySelector('.donate, .donateImg').onclick = function(e) {
                    e.preventDefault(this.getAttribute('href'));
                    console.log()
                    durl = this.getAttribute('href');
                    console.log('Saving dp state');
                    $body[0].classList.remove('popupOpen');
                    var oneday = new Date().setHours(new Date().getHours() + 24);
                    if (typeof(Storage) !== "undefined") {
                        localStorage.popupCheck = oneday;
                    }
                    window.open(durl, '_blank');
                    // send event to GA
                    ga("send", "event", "Lighboxes", "Popup Clicked", lightboxTitle[activeArray], {
                        hitCallback: function() {
                            console.log('event sent');
                        }
                    });
                }
            }, 500);
        }, 4000)
    }
}



/* ------------
EE 404 JS ---------------- */

/* Wrap div.content with custom elements
----- FOLLOWING CODE IS BEING RUN ON 404 PAGE EMBED WIDGET
org_html = document.getElementsByClassName("content")[0].outerHTML;
new_html = "<div id='wall404'><div class='inner404'>" + org_html + "</div></div>";
document.getElementsByClassName("content")[0].outerHTML = new_html;
*/

/* elements */
$inner404 = document.getElementsByClassName('inner404')[0];

/* Wall click counter */
countClicks = function() {
    // define var
    if (typeof wallClicks === 'undefined') {
        wallClicks = 0;
    }
    // exit function if click limit passed
    else {
        if (wallClicks > 14) {
            return false;
        }
    }

    // reset timer
    if (typeof wallClickTimeout !== 'undefined') {
        clearTimeout(wallClickTimeout);
    }

    // increase click count
    wallClicks++;
    switch (wallClicks) {
        case 5:
            $inner404.classList.add('tap');
            customTimer(3000);
            break;
        case 8:
            $inner404.classList.remove('tap');
            $inner404.classList.add('knock');
            customTimer(3000);
            break;
        case 14:
            $inner404.classList.add('push');
            clearTimeout(wallClickTimeout);
            injectVid();
            return false;
            break;
    }

    // start timer
    wallClickTimeout = setTimeout(function() {
        wallClicks = 0;
        // $('.inner404').removeClass('tap knock');
    }, 1000);

    // console.log(wallClicks);
}

customTimer = function(time) {
    if (typeof newCusTimer !== 'undefined') {
        clearTimeout(newCusTimer);
    }
    newCusTimer = setTimeout(function() {
        $inner404.classList.remove('tap', 'knock');
    }, time);
}

injectVid = function() {
    eevc = document.createElement('div');
    eevc.setAttribute("id", "eevContainer");
    eevid = document.createElement('video');
    eevid.setAttribute("id", "eev");
    eevid.autoPlay = true;
    eesrc = document.createElement('source');
    eesrc.src = 'https://events.kansascitypbs.org/404EE-web.mp4';
    eesrc.type = "video/mp4";
    eevid.appendChild(eesrc);
    eevc.appendChild(eevid);
    $wall404.appendChild(eevc);
    document.getElementById("eev").play();
    $eevc = document.getElementById("eevContainer")
    setTimeout(function() {
        $eevc.classList.add('start');
    }, 50);
    setTimeout(function() {
        $eevc.classList.add('front');
        $wall404.style.background = "black";
        $inner404.style.opacity = "0.2";
    }, 5000);
}

/* initial wall click function */
$wall404 = document.getElementById("wall404");
if ($wall404) {
    $wall404.addEventListener("click", countClicks);
}
